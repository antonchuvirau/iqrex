import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'


@Injectable()
export class RegisterService {
	private http: HttpClient;

	constructor(http: HttpClient) {
		this.http = http;
	}

	public getCountries(): Observable<any[]> {
		return this.http.get('../assets/countries.json')
			.map((x: any) => <any[]>x);
	}
}
