import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';
import { RegisterService } from "./countries.service";

/**
 * Generated class for the CountriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-countries',
  templateUrl: 'countries.html',
})
export class CountriesPage implements OnInit {

  public countries: any[];
  private service: RegisterService;

  constructor(public params: NavParams, public viewCtrl: ViewController, service: RegisterService) {
    this.service = service;
  }

  ngOnInit(): void {
    this.service.getCountries()
      .subscribe(countries => {
        this.countries = countries;
      });
  }

  select(country){
    this.viewCtrl.dismiss(country);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
