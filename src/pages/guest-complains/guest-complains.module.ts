import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuestComplainsPage } from './guest-complains';

@NgModule({
  declarations: [
    GuestComplainsPage,
  ],
  imports: [
    IonicPageModule.forChild(GuestComplainsPage),
  ],
})
export class GuestComplainsPageModule {}
