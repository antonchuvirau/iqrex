import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplainMapPage } from './complain-map';

@NgModule({
  declarations: [
    ComplainMapPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplainMapPage),
  ],
})
export class ComplainMapPageModule {}
