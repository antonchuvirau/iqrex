import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComplainEditPage } from './complain-edit';

@NgModule({
  declarations: [
    ComplainEditPage,
  ],
  imports: [
    IonicPageModule.forChild(ComplainEditPage),
  ],
})
export class ComplainEditPageModule {}
