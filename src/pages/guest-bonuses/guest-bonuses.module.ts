import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuestBonusesPage } from './guest-bonuses';

@NgModule({
  declarations: [
    GuestBonusesPage,
  ],
  imports: [
    IonicPageModule.forChild(GuestBonusesPage),
  ],
})
export class GuestBonusesPageModule {}
