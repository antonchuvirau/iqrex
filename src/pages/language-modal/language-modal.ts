import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ViewOrContainerState } from '@angular/core/src/render3/interfaces';

/**
 * Generated class for the LanguageModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-language-modal',
  templateUrl: 'language-modal.html',
})
export class LanguageModalPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LanguageModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
