import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanguageModalPage } from './language-modal';

@NgModule({
  declarations: [
    LanguageModalPage,
  ],
  imports: [
    IonicPageModule.forChild(LanguageModalPage),
  ],
})
export class LanguageModalPageModule {}
