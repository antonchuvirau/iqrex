import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmptyProductPage } from './empty-product';

@NgModule({
  declarations: [
    EmptyProductPage,
  ],
  imports: [
    IonicPageModule.forChild(EmptyProductPage),
  ],
})
export class EmptyProductPageModule {}
