import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';

import { LanguageModalPage } from '../../language-modal/language-modal';
import { CountriesPage } from '../../countries/countries';
/**
 * Generated class for the EnterPhoneNumberPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-enter-phone-number',
  templateUrl: 'enter-phone-number.html',
})
export class EnterPhoneNumberPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnterPhoneNumberPage');
  }

  openLanguages() {

    let modal = this.modalCtrl.create(LanguageModalPage);
    modal.present();
  }

  selectCountry() {

    let profileModal = this.modalCtrl.create(CountriesPage);
    profileModal.present();
  }


}
