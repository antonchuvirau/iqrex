import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnterPhoneNumberPage } from './enter-phone-number';

@NgModule({
  declarations: [
    EnterPhoneNumberPage
  ],
  imports: [
    IonicPageModule.forChild(EnterPhoneNumberPage),
  ]
})
export class EnterPhoneNumberPageModule {}
