import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuestSettingsPage } from './guest-settings';

@NgModule({
  declarations: [
    GuestSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(GuestSettingsPage),
  ],
})
export class GuestSettingsPageModule {}
