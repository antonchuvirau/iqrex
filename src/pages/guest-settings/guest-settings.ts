import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { LanguageModalPage } from '../language-modal/language-modal';
/**
 * Generated class for the GuestSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guest-settings',
  templateUrl: 'guest-settings.html',
})
export class GuestSettingsPage {

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GuestSettingsPage');
  }

  openLanguages() {

    let modal = this.modalCtrl.create(LanguageModalPage);
    modal.present();
  }

}
