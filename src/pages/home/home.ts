import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { LanguageModalPage } from '../language-modal/language-modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public modalCtrl: ModalController, public navCtrl: NavController) {

  }
  openPage(item) {
    this.navCtrl.push(item);
  }

  openLanguages() {

    let modal = this.modalCtrl.create(LanguageModalPage);
    modal.present();
  }

}
