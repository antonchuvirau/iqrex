import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NavController } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { LanguageModalPage } from '../pages/language-modal/language-modal';

//Pages
import { EnterPhoneNumberPage } from '../pages/registration/enter-phone-number/enter-phone-number';
import { EnterCodePage } from '../pages/registration/enter-code/enter-code';
import { LoginPage } from '../pages/login/login';
import { HistoryPage } from '../pages/history/history';
import { ProductPage } from '../pages/product/product';
import { EmptyProductPage } from '../pages/empty-product/empty-product';
import { ComplainEditPage } from '../pages/complain-edit/complain-edit';
import { ComplainMapPage } from '../pages/complain-map/complain-map';
import { BonusesPage } from '../pages/bonuses/bonuses';
import { SettingsPage } from '../pages/settings/settings';
import { ProfileEditPage } from '../pages/profile-edit/profile-edit';
import { GuestComplainsPage } from '../pages/guest-complains/guest-complains';
import { GuestBonusesPage } from '../pages/guest-bonuses/guest-bonuses';
import { GuestSettingsPage } from '../pages/guest-settings/guest-settings';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Enter PhoneNumber Page', component: EnterPhoneNumberPage },
      { title: 'Enter Code Page', component: EnterCodePage },
      { title: 'Login page', component: LoginPage },
      { title: 'History Page', component: HistoryPage },
      { title: 'Product Page', component: ProductPage },
      { title: 'Empty Product Page', component: EmptyProductPage },
      { title: 'Complain Edit Page', component: ComplainEditPage },
      { title: 'Complain Map Page', component: ComplainMapPage },
      { title: 'Bonuses Page', component: BonusesPage },
      { title: 'Settings Page', component: SettingsPage },
      { title: 'Profile Edit Page', component: ProfileEditPage },
      { title: 'Guest Complains Page', component: GuestComplainsPage },
      { title: 'Guest Bonuses Page', component: GuestBonusesPage },
      { title: 'Guest Settings Page', component: GuestSettingsPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

