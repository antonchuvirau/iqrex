import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule  } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LanguageModalPage } from '../pages/language-modal/language-modal';
import { CountriesPage } from '../pages/countries/countries';

//Pages
import { EnterPhoneNumberPage } from '../pages/registration/enter-phone-number/enter-phone-number';
import { EnterCodePage } from '../pages/registration/enter-code/enter-code';
import { LoginPage } from '../pages/login/login';
import { HistoryPage } from '../pages/history/history';
import { ProductPage } from '../pages/product/product';
import { EmptyProductPage } from '../pages/empty-product/empty-product';
import { ComplainEditPage } from '../pages/complain-edit/complain-edit';
import { ComplainMapPage } from '../pages/complain-map/complain-map';
import { BonusesPage } from '../pages/bonuses/bonuses';
import { ScanPage } from '../pages/scan/scan';
import { SettingsPage } from '../pages/settings/settings';
import { ProfileEditPage } from '../pages/profile-edit/profile-edit';
import { GuestComplainsPage } from '../pages/guest-complains/guest-complains';
import { GuestBonusesPage } from '../pages/guest-bonuses/guest-bonuses';
import { GuestSettingsPage } from '../pages/guest-settings/guest-settings';
import { RegisterService } from "../pages/countries/countries.service";

@NgModule({
  declarations: [
    MyApp,
    LanguageModalPage,
    EnterPhoneNumberPage,
    EnterCodePage,
    HomePage,
    LoginPage,
    HistoryPage,
    ProductPage,
    EmptyProductPage,
    ComplainEditPage,
    ComplainMapPage,
    BonusesPage,
    ScanPage,
    SettingsPage,
    ProfileEditPage,
    GuestComplainsPage,
    GuestBonusesPage,
    GuestSettingsPage,
    CountriesPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {}, {
      links: [
       { component: EnterPhoneNumberPage, name: 'Enter Phone Number Page', segment: 'page-2' },
       { component: EnterCodePage, name: 'Enter Code Page', segment: 'page-3' },
       { component: HomePage, name: 'Home Page', segment: 'page-1' },
       { component: LoginPage, name: 'Login Page', segment: 'page-4' },
       { component: HistoryPage, name: 'History Page', segment: 'page-5' },
       { component: ProductPage, name: 'Product Page', segment: 'page-6' },
       { component: EmptyProductPage, name: 'Empty Product Page', segment: 'page-7' },
       { component: ComplainEditPage, name: 'Complain Edit Page', segment: 'page-8' },
       { component: ComplainMapPage, name: 'Complain Map Page', segment: 'page-9' },
       { component: BonusesPage, name: 'Bonuses Page', segment: 'page-10' },
       { component: ScanPage, name: 'Scan Page', segment: 'page-11' },
       { component: SettingsPage, name: 'Settings Page', segment: 'page-12' },
       { component: ProfileEditPage, name: 'Profile Edit Page', segment: 'page-13' },
       { component: GuestComplainsPage, name: 'Guest Complains Page', segment: 'page-14' },
       { component: GuestBonusesPage, name: 'Guest Bonuses Page', segment: 'page-15' },
       { component: GuestSettingsPage, name: 'Guest Settings Page', segment: 'page-16' }
     ]
   })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LanguageModalPage,
    EnterPhoneNumberPage,
    EnterCodePage,
    HomePage,
    LoginPage,
    HistoryPage,
    ProductPage,
    EmptyProductPage,
    ComplainEditPage,
    ComplainMapPage,
    BonusesPage,
    ScanPage,
    SettingsPage,
    ProfileEditPage,
    GuestComplainsPage,
    GuestBonusesPage,
    GuestSettingsPage,
    CountriesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClientModule,
    RegisterService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
